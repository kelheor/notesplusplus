package com.academy.notes.ui.activity;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import com.academy.notes.R;
import com.academy.notes.service.NoteService;
import com.academy.notes.service.impl.NoteServiceImpl;

/**
 * @author: Руслан
 */
public class NotesActivity extends Activity {

    private NoteService noteService;

    private static final String TAG = "NOTES++";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        noteService = new NoteServiceImpl(this.getApplicationContext());
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

}
