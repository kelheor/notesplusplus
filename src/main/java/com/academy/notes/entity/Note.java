package com.academy.notes.entity;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * @author: Руслан
 */
@DatabaseTable(tableName = "notes")
public class Note {

    @DatabaseField(generatedId = true, columnName = "note_id")
    private int noteId;

    @DatabaseField(columnName = "text", dataType= DataType.BYTE_ARRAY)
    private byte[] text;

    public int getNoteId() {
        return noteId;
    }

    public void setNoteId(int noteId) {
        this.noteId = noteId;
    }

    public byte[] getText() {
        return text;
    }

    public void setText(byte[] text) {
        this.text = text;
    }

    public String getTextString() {
        return new String(text);
    }

    public void setTextString(String text) {
        this.text = text.getBytes();
    }

    public Note() {
    }


}
