package com.academy.notes.database.utils;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.io.*;

/**
 * @author: Руслан
 */
public class DatabaseInitializer extends SQLiteOpenHelper {

    private static String DB_PATH = "/data/data/com.academy.notes/databases/";
    private static String DB_NAME = "db.sqlite";

    private SQLiteDatabase database;
    private final Context context;

    private static final String TAG = "NOTES" + "." + DatabaseHelper.class.getName();

    public DatabaseInitializer(Context context) {
        super(context, DB_NAME, null, 1);
        this.context = context;
    }

    public void createDatabase() throws IOException {

        boolean dbExist = checkDatabase();

        if (!dbExist) {
            this.getReadableDatabase();
            try {
                copyDatabase();
            } catch (IOException e) {
                Log.e(TAG, e.getMessage(), e);
                throw new Error("Error copying database");
            }
        }

    }

    private boolean checkDatabase() {

        boolean checkdb = false;
        try{
            String myPath = context.getFilesDir().getAbsolutePath().replace("files", "databases")+File.separator + DB_NAME;
            File dbfile = new File(myPath);
            checkdb = dbfile.exists();
        }
        catch(SQLiteException e){
            System.out.println("Database doesn't exist");
        }

        return checkdb;
    }


    private void copyDatabase() throws IOException {
        InputStream myInput = null;
        OutputStream myOutput = null;
        try {

            String outFileName = context.getFilesDir().getAbsolutePath().replace("files", "databases")+File.separator + DB_NAME;

            myInput = context.getAssets().open(DB_NAME);

            myOutput = new FileOutputStream(outFileName);

            byte[] buffer = new byte[1024];
            int length;
            while ((length = myInput.read(buffer)) > 0) {
                myOutput.write(buffer, 0, length);
            }

            myOutput.flush();
        } finally {
            if(myOutput != null) {
                myOutput.close();
            }
            if(myInput != null) {
                myInput.close();
            }
        }

    }

    @Override
    public synchronized void close() {
        if (database != null)
            database.close();

        super.close();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }

}

