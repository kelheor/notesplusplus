package com.academy.notes.service;

import com.academy.notes.entity.Note;

/**
 * @author: Kelheor
 */
public interface NoteService {
    public Note createNote(Note note);
    public Note getNoteById(int id);
}
