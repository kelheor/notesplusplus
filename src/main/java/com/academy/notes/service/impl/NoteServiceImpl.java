package com.academy.notes.service.impl;

import android.content.Context;
import com.academy.notes.dao.NoteDAO;
import com.academy.notes.dao.impl.NoteDAOImpl;
import com.academy.notes.database.utils.DatabaseHelper;
import com.academy.notes.database.utils.DatabaseManager;
import com.academy.notes.entity.Note;
import com.academy.notes.service.NoteService;

/**
 * @author: Kelheor
 */
public class NoteServiceImpl implements NoteService {

    private DatabaseHelper db;

    private NoteDAO noteDAO;

    public NoteServiceImpl(Context context) {
        DatabaseManager<DatabaseHelper> manager = new DatabaseManager<DatabaseHelper>();
        db = manager.getHelper(context);
        noteDAO = new NoteDAOImpl(db);
    }

    public Note createNote(Note note) {
        noteDAO.create(note);
        return note;
    }

    public Note getNoteById(int id) {
        Note note = noteDAO.getById(id);
        return note;
    }
}
