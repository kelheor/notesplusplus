package com.academy.notes.dao.impl;

import android.util.Log;
import com.academy.notes.dao.NoteDAO;
import com.academy.notes.database.utils.DatabaseHelper;
import com.academy.notes.entity.Note;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;

import java.sql.SQLException;
import java.util.List;

/**
 * @author: Руслан
 */
public class NoteDAOImpl implements NoteDAO {

    private Dao<Note, String> noteDao;

    private static final String TAG = "NOTES" + "." + NoteDAOImpl.class.getName();

    public NoteDAOImpl(DatabaseHelper db) {
        try {
            noteDao = DaoManager.createDao(db.getConnectionSource(), Note.class);
        } catch (SQLException e) {
            Log.e(TAG, e.getMessage(), e);
        }
    }

    @Override
    public int create(Note note) {
        try {
            return noteDao.create(note);
        } catch (SQLException e) {
            Log.e(TAG, e.getMessage(), e);
        }
        return 0;
    }

    @Override
    public int update(Note note) {
        try {
            return noteDao.update(note);
        } catch (SQLException e) {
            Log.e(TAG, e.getMessage(), e);
        }
        return 0;
    }

    @Override
    public int delete(Note note) {
        try {
            return noteDao.delete(note);
        } catch (SQLException e) {
            Log.e(TAG, e.getMessage(), e);
        }
        return 0;
    }

    @Override
    public Note getById(int id) {
        try {
            QueryBuilder<Note, String> qb = noteDao.queryBuilder();

            qb.where().eq("note_id", id);

            PreparedQuery<Note> pq = qb.prepare();
            return noteDao.queryForFirst(pq);
        } catch (SQLException e) {
            Log.e(TAG, e.getMessage(), e);
        }
        return null;
    }

    @Override
    public List<Note> getAll() {
        try {
            return noteDao.queryForAll();
        } catch (SQLException e) {
            Log.e(TAG, e.getMessage(), e);
        }
        return null;
    }
}
