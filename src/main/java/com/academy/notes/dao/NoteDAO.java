package com.academy.notes.dao;

import com.academy.notes.entity.Note;

import java.util.List;

/**
 * @author: Руслан
 */
public interface NoteDAO {
    public int create(Note note);
    public int update(Note note);
    public int delete(Note note);
    public Note getById(int id);
    public List<Note> getAll();

}
